//
//  AppDelegate.h
//  ContactsSample
//
//  Created by Neetika on 11/04/18.
//  Copyright © 2018 Neetika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

