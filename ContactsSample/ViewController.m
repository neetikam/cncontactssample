//
//  ViewController.m
//  ContactsSample
//
//  Created by Neetika on 11/04/18.
//  Copyright © 2018 Neetika. All rights reserved.
//

#import "ViewController.h"
#import <Contacts/Contacts.h>

#define kGroupName @"test contacts"

@interface ViewController ()

@property (nonatomic, strong) CNContactStore *store;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self askContactsPermission];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)askContactsPermission {
    self.store = [[CNContactStore alloc] init];
    __weak typeof(self) weakSelf = self;
    [self.store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            [weakSelf createGroup];
        }
    }];
}

- (void)saveContactToGroup:(CNMutableGroup *)group {
    CNSaveRequest *saveReq = [[CNSaveRequest alloc] init];
    CNMutableContact *contact = [self addContact];
    [saveReq addMember:contact toGroup:group];
    [saveReq addContact:contact toContainerWithIdentifier:self.store.defaultContainerIdentifier];
    NSError *err = nil;
    BOOL isSaved = [_store executeSaveRequest:saveReq error:&err];
    NSLog(@"Contact Saved: %d",isSaved);
}

- (void)createGroup {
    CNMutableGroup *group = [[CNMutableGroup alloc] init];
    [group setName:kGroupName];
    CNSaveRequest *saveReq = [[CNSaveRequest alloc] init];
    [saveReq addGroup:group toContainerWithIdentifier:self.store.defaultContainerIdentifier];
    NSError *err = nil;
    BOOL isSaved = [_store executeSaveRequest:saveReq error:&err];
    NSLog(@"Saved: %d",isSaved);
    if (isSaved) {
        [self saveContactToGroup:group];
    }
}

- (CNMutableContact *)addContact {
    CNMutableContact *contact = [[CNMutableContact alloc] init];
    contact.givenName = @"Test";
    CNPhoneNumber *phone = [CNPhoneNumber phoneNumberWithStringValue:@"123456789"];
    CNLabeledValue *value = [[CNLabeledValue alloc] initWithLabel:CNLabelPhoneNumberMobile value:phone];
    contact.phoneNumbers = @[value];
    return contact;
}

@end
